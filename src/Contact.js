import axios from "axios";
import { useState } from "react";

function ContactApp() {
  const [nama, setName] = useState("");
  const [kehadiran, setStatus] = useState("");
  const [ucapan, setMessage] = useState("");
  // const [timestamp, setTimeStamp] = useState("");

  // const [data, setData] = useState([]);

  const handleSubmit = (e) => {
    e.preventDefault();
    // console.log(name, age, designation, salary);

    // our object to pass
    const data = {
      timestamp: new Date(),
      nama,
      kehadiran,
      ucapan,
    };
    axios.post("https://sheet.best/api/sheets/ac82d94c-ffc3-426b-9dcb-4144259aced2", data).then((response) => {
      // console.log(response);

      setName("");
      setStatus("");
      setMessage("");
    });
  };

  // const getData = () => {
  //   axios.get("https://sheet.best/api/sheets/e7a8bead-e947-4de5-9421-8e17433a3fff").then((response) => {
  //     setData(response.data);
  //   });
  // };

  // useEffect(() => {
  //   getData();
  // }, [data]);

  return (
    <div className="col-lg-12 kontak">
      <div className="judul text-center">
        <h1 className="h1-judul">KONTAK</h1>
        <p>location information & attendance book</p>
      </div>

      <div className="card-container">
        <div className="row justify-content-center">
          <div className="col-lg-11 col-md-11 col-11 py-5 kontak-container card-radius mb-5">
            <div className="row justify-content-center">
              <div className="col-11">
                <div className="alert alert-success alert-dismissible fade show d-none my-alert" role="alert">
                  <strong>Terima Kasih !</strong> Pesan Berhasil Terkirim.
                  <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
              </div>
            </div>
            <div className="row justify-content-evenly">
              <div className="col-lg-5 col-md-5 col-11">
                <iframe
                  title="google map"
                  className="card-radius maps"
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3943.894616656512!2d115.21712121529683!3d-8.701557091070834!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd24110b802d94d%3A0xb6246c32603fed5f!2sJl.%20Raya%20Sesetan%20No.516%2C%20Sesetan%2C%20Denpasar%20Selatan%2C%20Kota%20Denpasar%2C%20Bali%2080223!5e0!3m2!1sid!2sid!4v1644219777731!5m2!1sid!2sid"
                  width="100%"
                  height="100%"
                  allowFullScreen=""
                  loading="lazy"
                ></iframe>
              </div>

              <div className="col-lg-5 col-md-5 col-11 mb-5">
                <form className="form-container" name="weding" onSubmit={handleSubmit}>
                  <div className="form-floating mb-4">
                    <input type="text" className="form-control" id="nama" name="nama" placeholder="nama lengkap" onChange={(e) => setName(e.target.value)} value={nama} />
                    <label htmlFor="nama">Nama Lengkap</label>
                  </div>
                  <div className="form-floating mb-4">
                    <select className="form-select" id="kehadiran" name="kehadiran" onChange={(e) => setStatus(e.target.value)}>
                      <option defaultValue="Pilih Kehadiran" onChange={(e) => setStatus("Hadir")} value={kehadiran}>
                        Pilih Kehadiran
                      </option>
                      <option onChange={(e) => setStatus("Hadir")} value="Hadir">
                        Hadir
                      </option>
                      <option onChange={(e) => setStatus("Masih Ragu")} value="Masih Ragu">
                        Masih Ragu
                      </option>
                      <option onChange={(e) => setStatus("Tidak Hadir")} value="Tidak Hadir">
                        Tidak Hadir
                      </option>
                    </select>
                    <label htmlFor="kehadiran">Status Kehadiran</label>
                  </div>
                  <div className="form-floating mb-4">
                    <textarea className="form-control" id="ucapan" name="ucapan" placeholder="ucapan" style={{ height: `150px` }} onChange={(e) => setMessage(e.target.value)} value={ucapan}></textarea>
                    <label htmlFor="ucapan">Ucapan</label>
                  </div>
                  <button type="submit" className="btn btn-dark float-end btn-kirim" name="submit">
                    Kirim Ucapan
                  </button>
                  <button className="btn btn-loading float-end btn-dark border-0 d-none bg-success" type="button" disabled>
                    <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                    Loading...
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ContactApp;
