import BodyApp from "./BodyApp";
import oratas from "./img/oratas.png";
import orbawah from "./img/orbawah.png";

function HeadApp() {
  return (
    <section id="content">
      <div className="container-fluid">
        <div className="row silde-satu">
          <div className="col-lg-12 m-0 p-0 w-100">
            {/* <div className="hero__box-left"></div> */}
            <div id="carousel-satu" className="carousel slide carousel-fade" data-bs-ride="carousel">
              <div className="carousel-inner">
                <div className="carousel-item d-flex justify-content-center active" data-bs-interval="10000" style={{ backgroundImage: `url('https://github.com/smkbelajarid/weding/blob/main/asset/img/cr1.jpg?raw=true')` }}>
                  <div className="row align-items-center">
                    <div className="deskripsi">
                      <div className="row justify-content-center">
                        <div className="col-lg-6 col-mg-6 col-6">
                          <img src={oratas} className="img-ornament" alt="" />
                        </div>
                      </div>
                      <div className="row justify-content-center">
                        <div className="col-lg-12">
                          <h1 className="deskripsi-nama">Yanto & Yanti</h1>
                        </div>
                      </div>
                      <div className="row justify-content-center">
                        <div className="col-lg-6 col-md-6 col-6">
                          <img src={orbawah} className="img-ornament" alt="" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="carousel-item d-flex justify-content-center" data-bs-interval="10000" style={{ backgroundImage: `url('https://github.com/smkbelajarid/weding/blob/main/asset/img/cr1.jpg?raw=true')` }}>
                  <div className="row align-items-center">
                    <div className="deskripsi">
                      <div className="row justify-content-center">
                        <div className="col-lg-5 col-mg-6 col-6">
                          <img src={oratas} className="img-ornament" alt="" />
                        </div>
                      </div>
                      <div className="row justify-content-center">
                        <div className="col-lg-12">
                          <h1 className="deskripsi-nama">Yanto & Yanti</h1>
                        </div>
                      </div>
                      <div className="row justify-content-center">
                        <div className="col-lg-5 col-md-6 col-6">
                          <img src={orbawah} className="img-ornament" alt="" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="carousel-item d-flex justify-content-center" data-bs-interval="10000" style={{ backgroundImage: `url('https://github.com/smkbelajarid/weding/blob/main/asset/img/cr1.jpg?raw=true')` }}>
                  <div className="row align-items-center">
                    <div className="deskripsi">
                      <div className="row justify-content-center">
                        <div className="col-lg-6 col-mg-6 col-6">
                          <img src={oratas} className="img-ornament" alt="" />
                        </div>
                      </div>
                      <div className="row justify-content-center">
                        <div className="col-lg-12">
                          <h1 className="deskripsi-nama">Yanto & Yanti</h1>
                        </div>
                      </div>
                      <div className="row justify-content-center">
                        <div className="col-lg-6 col-md-6 col-6">
                          <img src={orbawah} className="img-ornament" alt="" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <BodyApp />
      </div>
    </section>
  );
}

export default HeadApp;
