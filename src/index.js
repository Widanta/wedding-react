import React from "react";
import ReactDOM from "react-dom";
// import "./index.css";
// import App from "./App";
import HeadApp from "./HeadApp";
// import BodyApp from "./BodyApp";
import reportWebVitals from "./reportWebVitals";

ReactDOM.render(
  <React.StrictMode>
    {/* <App /> */}
    <HeadApp />
    {/* <BodyApp /> */}
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
