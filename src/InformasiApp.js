function InformasiApp() {
  return (
    <div className="col-lg-12 acara">
      <div className="judul text-center">
        <h1 className="h1-judul">ACARA</h1>
        <p>Ceremony Information</p>
      </div>
      <div className="card-container-acara mt-5">
        <div className="row justify-content-center">
          <div className="col-lg-3 col-md-4">
            <div className="card text-center bg-transparent border-0">
              <p className="fs-1">
                <i className="bi bi-calendar2-week-fill"></i>
              </p>
              <div className="card-body">
                <p className="card-text">Tanggal</p>
                <h3 className="card-title">23 Februari 2022</h3>
              </div>
            </div>
          </div>

          <div className="col-lg-3 col-md-4">
            <div className="card text-center bg-transparent border-0">
              <p className="fs-1">
                <i className="bi bi-clock-fill"></i>
              </p>
              <div className="card-body">
                <p className="card-text">Waktu</p>
                <h3 className="card-title">09.00 - Selesai</h3>
              </div>
            </div>
          </div>

          <div className="col-lg-3 col-md-4">
            <div className="card text-center bg-transparent border-0">
              <p className="fs-1">
                <i className="bi bi-geo-alt-fill"></i>
              </p>
              <div className="card-body">
                <p className="card-text">Alamat</p>
                <h3 className="card-title">Jalan Raya Sesetan No 516</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default InformasiApp;
