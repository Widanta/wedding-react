import cr1 from "./img/cr1.jpg";
import cr2 from "./img/cr2.jpg";
import cr3 from "./img/cr3.jpg";

function CountDownApp() {
  return (
    <div className="col-lg-12 waktu m-0 p-0">
      <div id="carousel-dua" className="carousel slide carousel-fade" data-bs-ride="carousel">
        <div className="carousel-inner">
          <div className="carousel-item carousel-item-dua active" data-bs-interval="10000" style={{ backgroundImage: `url(${cr1})` }}>
            <div className="carousel-caption">
              <div className="row justify-content-center">
                <div className="col-lg-1 col-md-2 col-3">
                  <h1 className="fw-bold">01</h1>
                  <p>Hari</p>
                </div>
                <div className="col-lg-1 col-md-2 col-3">
                  <h1 className="fw-bold">12</h1>
                  <p>Jam</p>
                </div>
                <div className="col-lg-1 col-md-2 col-3">
                  <h1 className="fw-bold">52</h1>
                  <p>Menit</p>
                </div>
                <div className="col-lg-1 col-md-2 col-3">
                  <h1 className="fw-bold">04</h1>
                  <p>Detik</p>
                </div>
              </div>
            </div>
          </div>
          <div className="carousel-item carousel-item-dua" data-bs-interval="2000" style={{ backgroundImage: `url(${cr2})` }}>
            <div className="carousel-caption">
              <div className="row justify-content-center">
                <div className="col-lg-1 col-md-2 col-3">
                  <h1 className="fw-bold">01</h1>
                  <p>Hari</p>
                </div>
                <div className="col-lg-1 col-md-2 col-3">
                  <h1 className="fw-bold">12</h1>
                  <p>Jam</p>
                </div>
                <div className="col-lg-1 col-md-2 col-3">
                  <h1 className="fw-bold">52</h1>
                  <p>Menit</p>
                </div>
                <div className="col-lg-1 col-md-2 col-3">
                  <h1 className="fw-bold">04</h1>
                  <p>Detik</p>
                </div>
              </div>
            </div>
          </div>
          <div className="carousel-item carousel-item-dua" data-bs-interval="2000" style={{ backgroundImage: `url(${cr3})` }}>
            <div className="carousel-caption">
              <div className="row justify-content-center">
                <div className="col-lg-1 col-md-2 col-3">
                  <h1 className="fw-bold">01</h1>
                  <p>Hari</p>
                </div>
                <div className="col-lg-1 col-md-2 col-3">
                  <h1 className="fw-bold">12</h1>
                  <p>Jam</p>
                </div>
                <div className="col-lg-1 col-md-2 col-3">
                  <h1 className="fw-bold">52</h1>
                  <p>Menit</p>
                </div>
                <div className="col-lg-1 col-md-2 col-3">
                  <h1 className="fw-bold">04</h1>
                  <p>Detik</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CountDownApp;
