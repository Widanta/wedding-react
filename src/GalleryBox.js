import cowok from "./img/cowok.jpg";
// import cewek from "./img/cewek.jpg";
import cr1 from "./img/cr1.jpg";
import cr2 from "./img/cr2.jpg";

function GalleryBox() {
  return (
    <div className="col-lg-12 galery">
      <div className="judul text-center">
        <h1 className="h1-judul">GALLERY</h1>
        <p>Photo & Video Collection</p>
      </div>
      <div className="card-container">
        <div className="row justify-content-center">
          <div className="col-lg-11 col-md-11">
            <div className="row justify-content-center">
              <div className="col-lg-4 col-md-6 col-6">
                <a href={cowok} data-fancybox="gallery" data-caption="Make Up by : Gek Ti">
                  {" "}
                  <img className="w-100 card-radius" src={cowok} alt="" />
                </a>

                <a href={cr1} data-fancybox="gallery" data-caption="Make Up by : Gek Ti">
                  {" "}
                  <img className="w-100 card-radius mt-4" src={cr1} alt="" />
                </a>

                <a href={cr2} data-fancybox="gallery" data-caption="Make Up by : Gek Ti">
                  {" "}
                  <img className="w-100 card-radius mt-4" src={cr2} alt="" />
                </a>
              </div>

              <div className="col-lg-4 d-none d-xl-block d-lg-block d-xxl-block">
                <a href={cr1} data-fancybox="gallery" data-caption="Make Up by : Gek Ti">
                  {" "}
                  <img className="w-100 card-radius" src={cr1} alt="" />
                </a>

                <a href={cowok} data-fancybox="gallery" data-caption="Make Up by : Gek Ti">
                  {" "}
                  <img className="w-100 card-radius mt-4" src={cowok} alt="" />
                </a>

                <a href={cr2} data-fancybox="gallery" data-caption="Make Up by : Gek Ti">
                  {" "}
                  <img className="w-100 card-radius mt-4" src={cr2} alt="" />
                </a>
              </div>

              <div className="col-lg-4 col-md-6 col-6">
                <a href={cr1} data-fancybox="gallery" data-caption="Make Up by : Gek Ti">
                  {" "}
                  <img className="w-100 card-radius" src={cr1} alt="" />
                </a>

                <a href={cr2} data-fancybox="gallery" data-caption="Make Up by : Gek Ti">
                  {" "}
                  <img className="w-100 card-radius mt-4" src={cr2} alt="" />
                </a>

                <a href={cowok} data-fancybox="gallery" data-caption="Make Up by : Gek Ti">
                  {" "}
                  <img className="w-100 card-radius mt-4" src={cowok} alt="" />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default GalleryBox;
