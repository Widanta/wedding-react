import GalleryBox from "./GalleryBox";
import cewek from "./img/cewek.jpg";
import cowok from "./img/cowok.jpg";
import omswasti from "./img/omswasti.png";
import omsanti from "./img/omsanti.png";
import InformasiApp from "./InformasiApp";
import CountDownApp from "./CountDown";
import ContactApp from "./Contact";

function BodyApp() {
  return (
    <div className="row content justify-content-center">
      <div className="col-lg-12 info text-center">
        <div className="row justify-content-center py-4">
          <div className="col-lg-2 col-md-3 col-6">
            <img src={omswasti} alt="" className="img-ornament" />
          </div>
        </div>

        <p>
          Atas Asung Kertha Wara Nugraha Ida Sang Hyang Widhi Wasa / Tuhan Yang Maha Esa, kami bermaksud mengundang Bapak/Ibu/Saudara/i <br />
          pada acara Pawiwahan (Pernikahan) putra-putri kami
        </p>

        <div className="card-container">
          <div className="row justify-content-center">
            <div className="col-lg-3 col-md-5 col-10 card-1" data-aos="fade-up" data-aos-duration="1000">
              <div className="card card-1 m-0 p-0 border-0 bg-transparent shadow-lg card-radius">
                <img src={cewek} className="img-content" alt="img-cewek" />
                <div className="card-body mt-3 text-center">
                  <h4 className="card-title card-nama mb-4">Ni Wayan Yanti Mahendri</h4>
                  <p className="card-text">Putri pertama dari pasangan</p>
                  <p className="card-text fw-bold mb-2">I Wayan Mudia & Ni kadek Meri</p>
                  <p className="card-text">
                    <i className="bi bi-geo-alt-fill"></i> Jalan Siulan No 12 Penatih
                  </p>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-1 text-center d-flex justify-content-center">
              <h1 className="acara-icons d-flex align-items-center">Dengan</h1>
            </div>

            <div className="col-lg-3 col-md-5 col-10 card-2" data-aos="fade-up" data-aos-duration="1000">
              <div className="card m-0 p-0 border-0 bg-transparent shadow-lg card-radius">
                <img src={cowok} className="img-content" alt="img-cowok" />
                <div className="card-body mt-3 text-center">
                  <h4 className="card-title mb-4 card-nama">Putu Ary Raditya</h4>
                  <p className="card-text">Putri pertama dari pasangan</p>
                  <p className="card-text fw-bold mb-2">I Ketut Murna & Ni Luh Murniasih</p>
                  <p className="card-text">
                    <i className="bi bi-geo-alt-fill"></i> Jalan Raya Sesetan No 516
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <p className="mt-5">
          Merupakan suatu kehormatan dan kebahagiaan bagi kami apabila Bapak/Ibu/Saudara/i berkenan hadir dan memberikan doa restu kepada putra-putri kami. <br />
          Atas kehadiran serta doa restu Bapak/Ibu/Saudara/i, kami sekeluarga mengucapkan terima kasih. Kami yang berbahagia.
        </p>
        <h1 className="h1-info">Yanto & Yanti</h1>
      </div>
      <InformasiApp />
      <GalleryBox />
      <div className="row-lg-12 syukur d-flex justify-content-center">
        <div className="align-items-center text-center">
          <p>
            Merupakan suatu kehormatan dan kebahagiaan bagi kami apabila Bapak/Ibu/Saudara/i berkenan hadir dan memberikan doa restu kepada putra-putri kami. <br />
            Atas kehadiran serta doa restu Bapak/Ibu/Saudara/i, kami sekeluarga mengucapkan terima kasih. Kami yang berbahagia.
          </p>

          <div className="row justify-content-center pb-4">
            <div className="col-lg-5 col-md-5 col-9">
              <img src={omsanti} alt="" className="img-ornament" />
            </div>
          </div>
        </div>
      </div>

      <CountDownApp />
      <ContactApp />
      <div className="col-lg-12 footer d-flex align-items-center justify-content-center">
        <p className="m-0 text-center">
          Created with <i className="bi bi-heart-fill text-danger"></i> by{" "}
          <a href="#" className="">
            cangnganten.com
          </a>
        </p>
      </div>
      {/* end */}
    </div>
  );
}

export default BodyApp;
